const koa = require('koa');
const app = koa();
const views = require('co-views');

const renderer = views(__dirname + '/views', {
  ext: 'ejs'
});

const user = {
  name: {
    first: 'Tobi',
    last: 'Holowaychuk'
  },
  species: 'ferret',
  age: 3
};

app.use(function*() {
  this.body = yield renderer('user', {user: user});
});

app.listen(process.argv[2]);
