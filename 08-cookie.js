const koa = require('koa');
const app = koa();

app.keys = ['secret', 'keys'];

app.use(function* (next) {
  let viewCount = this.cookies.get('view', {signed: true}) || 0;
  viewCount = parseInt(viewCount) + 1;
  this.body = `${viewCount} views`
  this.cookies.set('view', viewCount, {signed: true});
});

app.listen(process.argv[2]);
