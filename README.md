This is a repo of `kick-off-koa` answers.

# Note

The workshop covers:

* How to respond
* How to use simple route
* How to parse request body with `co-body` (More options see https://github.com/cojs/co-body)
* Throw error code and message with `this.throw` (`context.throw`)
* You can send `string`, `Buffer`, `JSON Object`, or `Stream` as response body
* Use `this.request.is()` to check if the request is `json` or something else.
* Inside a middleware, the statements before `yield next` will be run before the subapp (`next`) executes, while the statements after `yield next` will be executed after the subapp (`next`) executes.
* You may use try-catch to handle errors
* Use `context.cookies` to handle cookies.
* Use session with `koa-session`
* Templating with `ejs` and `co-views`
* Implement simple authentication with `co-body` and `koa-session` (simple form login).

## Possible bug of this workshop

In exercise 7 (error handling), there's a case problem in the final answer. I skipped this one.
