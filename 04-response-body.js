const koa = require('koa');
const fs = require('fs');
const app = koa();
const port = process.argv[2];

app.use(function* (next){
  if (this.path === '/json') {
    this.body = {foo: 'bar'};
  } else if (this.path === '/stream') {
    this.body = fs.createReadStream(process.argv[3]);
  }else {
    yield next;
  }
});

app.listen(port);
