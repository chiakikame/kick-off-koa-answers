const koa = require('koa');
const app = koa();
const port = process.argv[2];

app.use(function* (next){
  if (this.path === '/') {
    this.body = 'hello koa';
  } else {
    yield next;
  }
});

app.use(function* (next) {
  if (this.path === '/404') {
    this.body = 'page not found';
  } else {
    yield next;
  }
});

app.use(function* (next) {
  if (this.path === '/500') {
    this.body = 'internal server error';
  } else {
    yield next;
  }
});

app.listen(port);
