var koa = require('koa');
var session = require('koa-session');

var app = koa();
app.keys = ['secret', 'keys'];

app.use(session(app));

app.use(function*(next) {
  let viewCount = this.session.views || 0;
  viewCount = parseInt(viewCount) + 1;
  this.body = `${viewCount} views`;
  this.session.views = viewCount;
});

app.listen(process.argv[2]);
