const koa = require('koa');
const app = koa();
const parse = require('co-body');
const port = process.argv[2];

app.use(function* (next){
  if (this.method === 'POST' && this.path === '/') {
    const body = yield parse(this);
    this.body = body.name.toUpperCase();
  } else {
    yield next;
  }
});

app.listen(port);
